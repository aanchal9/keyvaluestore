//
//  KeyValueStoreViewController.m
//  KeyValueStore
//
//  Created by Aanchal Arora on 11/2/15.
//  Copyright (c) 2015 Aanchal. All rights reserved.
//

#import "KeyValueStoreViewController.h"
#import "StoreManager.h"

@interface KeyValueStoreViewController ()

@property (nonatomic, strong) StoreManager *manager;

@end

@implementation KeyValueStoreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.manager = [StoreManager sharedStoreManager];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)addKeyValuePairs:(id)sender {
    
    [self.manager setObject:@[@"A", @"B", @"C", @"D"] forKey:@"Alphabet"];
    [self.manager setObject:@"Admin" forKey:@"Admin"];
    [self.manager setObject:@[@"Apple", @"Mango", @"Banana"] forKey:@"Fruit"];
}

- (IBAction)getObjectForKey:(id)sender {
    
    NSArray *alphabets = [self.manager objectForKey:@"Alphabet"];
    NSLog(@"%@", alphabets);
    
}

- (IBAction)removeObjectForKey:(id)sender {
    
    [self.manager removeObjectForKey:@"Admin"];
    
    NSDictionary *dict = [self.manager loadStoreFromDatabase];
    
    NSLog(@"%@", dict);
}

- (IBAction)setValueForKey:(id)sender {
    
    [self.manager setObject:@[@"E", @"F", @"G", @"H"] forKey:@"Alphabet"];
    NSDictionary *dict = [self.manager loadStoreFromDatabase];
    
    NSLog(@"%@", dict);
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
