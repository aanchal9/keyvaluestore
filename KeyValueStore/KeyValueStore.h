// KeyValueStore.h


#import <Foundation/Foundation.h>

@protocol KeyValueStore <NSObject>
- (id) objectForKey:(NSString *)key;
- (void) setObject:(id)value forKey:(NSString *)key;
- (void) removeObjectForKey:(NSString *)key;
@end