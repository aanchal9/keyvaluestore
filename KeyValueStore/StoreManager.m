//
//  StoreManager.m
//  KeyValueStore
//
//  Created by Aanchal Arora on 11/1/15.
//  Copyright (c) 2015 Aanchal. All rights reserved.
//

#import "StoreManager.h"
#import <sqlite3.h>

@interface StoreManager ()

@property (nonatomic, strong) NSString *databaseFilename;

@end

@implementation StoreManager

- (instancetype)initWithFilename:(NSString *)filename
{
    self = [super init];
    if (self) {
        _databaseFilename = filename;
    }
    return self;
}

+ (StoreManager *)sharedStoreManager
{
    static dispatch_once_t onceToken;
    static StoreManager *instance = nil;
    dispatch_once(&onceToken, ^{
        instance = [[StoreManager alloc] initWithFilename:@"keyvaluestore.db"];
    });
    return instance;
}

- (NSMutableDictionary *)loadStoreFromDatabase
{
    sqlite3 *storedb;
    NSMutableDictionary *storeDictionary = [[NSMutableDictionary alloc] init];
    NSString *selectQuery = @"Select * from keyvaluestore";
    NSString *dbPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:self.databaseFilename];
    int result = sqlite3_open([dbPath UTF8String], &storedb);
    if (result == SQLITE_OK) {
        sqlite3_stmt *stmt;
        int ps_result = sqlite3_prepare_v2(storedb, [selectQuery UTF8String], -1, &stmt, NULL);
        if (ps_result == SQLITE_OK) {
            
            while (sqlite3_step(stmt) == SQLITE_ROW) {
                
                char *key = (char*)sqlite3_column_text(stmt, 0);
                NSString *keyString = [NSString stringWithUTF8String:key];
                
                const void* val = sqlite3_column_blob(stmt, 1);
                NSUInteger bytes = sqlite3_column_bytes(stmt, 1);
                
                NSData *valData = [NSData dataWithBytes:val length:bytes];
                
                id value = [NSKeyedUnarchiver unarchiveObjectWithData:valData];
                [storeDictionary setObject:value forKey:keyString];
            }
        }
        sqlite3_finalize(stmt);
    }
    sqlite3_close(storedb);
    return storeDictionary;
}

- (void)setObject:(id)value forKey:(NSString *)key
{
    
    NSData *valueData = [NSKeyedArchiver archivedDataWithRootObject:value];
    
    sqlite3 *storedb; int rowCount = 0;
    NSString *selectQuery = [NSString stringWithFormat:@"Select * from keyvaluestore where key = \"%@\"", key];
    NSString *dbPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:self.databaseFilename];
    int result = sqlite3_open([dbPath UTF8String], &storedb);
    if (result == SQLITE_OK) {
        sqlite3_stmt *stmt;
        int ps_result = sqlite3_prepare_v2(storedb, [selectQuery UTF8String], -1, &stmt, NULL);
        if (ps_result == SQLITE_OK) {
            
            while (sqlite3_step(stmt) == SQLITE_ROW) {
                rowCount++;
            }
            if (rowCount > 0)
            {
                NSString *updateQuery = @"Update keyvaluestore Set value = ? where key = ?";
                ps_result = sqlite3_prepare_v2(storedb, [updateQuery UTF8String], -1, &stmt, NULL);
                if (ps_result == SQLITE_OK) {
                    sqlite3_bind_text(stmt, 2, [key UTF8String], -1, NULL);
                    sqlite3_bind_blob(stmt, 1, [valueData bytes], (int)[valueData length], NULL);
			    @synchronized(self) {
                    int updateResult = sqlite3_step(stmt);
                    if (updateResult == SQLITE_DONE) {
                        NSLog(@"Updated");
                    }
                    else
                    {
                        NSLog(@"%s",sqlite3_errmsg(storedb));
                    }
			  }
                }
            }
            else
            {
                NSString *insertQuery = @"Insert into keyvaluestore values(?, ?)";
                ps_result = sqlite3_prepare_v2(storedb, [insertQuery UTF8String], -1, &stmt, NULL);
                if (ps_result == SQLITE_OK) {
                    sqlite3_bind_text(stmt, 1, [key UTF8String], -1, NULL);
                    sqlite3_bind_blob(stmt, 2, [valueData bytes], (int)[valueData length], NULL);
			    @synchronized(self) {
                    int insertResult = sqlite3_step(stmt);
                    if (insertResult == SQLITE_DONE) {
                        NSLog(@"Inserted");
                    }
                    else
                    {
                        NSLog(@"%s",sqlite3_errmsg(storedb));
                    }
			  }
                }

            }
        }
        sqlite3_finalize(stmt);
    }
    sqlite3_close(storedb);
}

- (id)objectForKey:(NSString *)key
{
    sqlite3 *storedb;
    id value;
    NSString *selectQuery = [NSString stringWithFormat:@"Select * from keyvaluestore where key = \"%@\"", key];
    NSString *dbPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:self.databaseFilename];
    int result = sqlite3_open([dbPath UTF8String], &storedb);
    if (result == SQLITE_OK) {
        sqlite3_stmt *stmt;
        int ps_result = sqlite3_prepare_v2(storedb, [selectQuery UTF8String], -1, &stmt, NULL);
        if (ps_result == SQLITE_OK) {
            
            sqlite3_step(stmt);
            const void* val = sqlite3_column_blob(stmt, 1);
            int bytes = sqlite3_column_bytes(stmt, 1);
            value = [NSKeyedUnarchiver unarchiveObjectWithData:[NSData dataWithBytes:val length:bytes]];
        }
        sqlite3_finalize(stmt);
    }
    sqlite3_close(storedb);
    return value;
}

- (void)removeObjectForKey:(NSString *)key
{
    sqlite3 *storedb;
    NSString *deleteQuery = [NSString stringWithFormat:@"Delete from keyvaluestore where key = \"%@\"", key];
    NSString *dbPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:self.databaseFilename];
    int result = sqlite3_open([dbPath UTF8String], &storedb);
    if (result == SQLITE_OK) {
        sqlite3_stmt *stmt;
        int ps_result = sqlite3_prepare_v2(storedb, [deleteQuery UTF8String], -1, &stmt, NULL);
        if (ps_result == SQLITE_OK) {
            @synchronized(self) {
            int deleteResult = sqlite3_step(stmt);
            if (deleteResult == SQLITE_DONE) {
                NSLog(@"Deleted");
            }
            else
            {
                NSLog(@"%s",sqlite3_errmsg(storedb));
            }
		}
        }
        sqlite3_finalize(stmt);
    }
    sqlite3_close(storedb);
}

@end
