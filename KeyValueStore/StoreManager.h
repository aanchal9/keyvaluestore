//
//  StoreManager.h
//  KeyValueStore
//
//  Created by Aanchal Arora on 11/1/15.
//  Copyright (c) 2015 Aanchal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KeyValueStore.h"

@interface StoreManager : NSObject <KeyValueStore>

+ (StoreManager*)sharedStoreManager;

- (NSMutableDictionary *)loadStoreFromDatabase;

@end
